(asdf:defsystem :llama-brain
  :description "PA brain based on llama.cpp"
  :version "0.0.1"
  :author "aragaer"
  :mailto "aragaer@gmail.com"
  :license "MIT"
  :depends-on (:cffi :claw-llama-cpp)
  :pathname "src/"
  :components ((:file "package")
               (:file "brain"))
  :serial t)
