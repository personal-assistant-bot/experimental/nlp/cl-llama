;;; -*- mode:common-lisp -*-

(in-package #:llama-brain)

(let ((args (cddr sb-ext:*posix-argv*)))
  (defvar *prompt*
    (if args
        (format nil "~{~a~^ ~}" args)
        "Hello, world!")))

(defcallback null-log :void ((level %llama-cpp:ggml-log-level)
                             (text :pointer)
                             (data :pointer))
  nil)

(%llama-cpp:log-set (callback null-log) (null-pointer))

(brain-init)

(defvar llama (make-instance 'llama
                             :context *context*
                             :n-ctx 4096
                             :batch-size 512))

(format t "~a" *prompt*)
(with-foreign-object (single-batch '(:struct %llama-cpp:batch))
  (%llama-cpp:batch-init single-batch 1 0 1)
  (unwind-protect
       (let ((prompt-len (feed *context* *model* *prompt* 512 4096))
             (eos (%llama-cpp:token-eos *context*))
             (limit 100))
         (loop for seq-id = prompt-len then (1+ seq-id)
               for idx = (1- prompt-len) then 0
               for token = (get-candidate *context* *top-k* *top-p* *temp* idx)
               until (= token eos)
               while (< seq-id 4096)
               do (progn
                    (format t "~a" (detokenize *model* token))
                    (force-output)
                    (fill-batch single-batch (list token) seq-id)
                    (%llama-cpp:decode *context* single-batch))))
    (%llama-cpp:batch-free single-batch)))

(brain-deinit)

(format t "~&")
