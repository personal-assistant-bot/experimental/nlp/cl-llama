;;; -*- mode:common-lisp -*-

(in-package #:llama-brain)

(defparameter *models-path* "/var/extra2/llama_models/")
(defparameter *model-file* "mistral-7b-v0.1.Q4_K_M.gguf")
(defparameter *n-ctx* 4096)
(defparameter *rope-base* 20000.0)
(defparameter *rope-scale* 0.95)

(defvar *model-params* (cl-llama:make-model-params '(%llama-cpp:main-gpu 0)))
(defvar *context-params* (cl-llama:make-context-params
                          `(%llama-cpp:n-ctx ,*n-ctx*
                            %llama-cpp:rope-freq-base ,*rope-base*
                            %llama-cpp:rope-freq-scale ,*rope-scale*)))

(defclass llama ()
    ((context
      :initarg :context
      :accessor context)
     (last-pos
      :initform 0
      :accessor last-pos)
     (n-ctx
      :initarg :n-ctx
      :accessor n-ctx)
     (batch-size
      :initarg :batch-size
      :accessor batch-size)))

(defmethod decode ((llama llama) prompt)
  (let* ((context (context llama))
         (model (%llama-cpp:get-model context))
         (batch-size (or (batch-size llama) (+ 2 (length prompt))))
         (n-ctx (or (n-ctx llama) (%llama-cpp:n-ctx-train model)))
         (pos (last-pos llama)))
    (with-foreign-object (batch '(:struct %llama-cpp:batch))
      (%llama-cpp:batch-init batch batch-size 0 1)
      (unwind-protect
           (with-foreign-slots ((%llama-cpp:n-tokens
                                 %llama-cpp:token
                                 %llama-cpp:pos
                                 %llama-cpp:n-seq-id
                                 %llama-cpp:seq-id
                                 (:pointer %llama-cpp:logits))
                                batch
                                %llama-cpp:batch)
             (let ((count (tokenize model prompt n-ctx %llama-cpp:token))
                   (logits (mem-ref %llama-cpp:logits :pointer)))
               ;; all tokens but last
               (loop for i from 0 to (- count 2)
                     do (setf (mem-aref %llama-cpp:pos '%llama-cpp:pos i) (+ i pos)
                              (mem-aref %llama-cpp:seq-id '%llama-cpp:seq-id i) 0
                              (mem-aref logits :int8 i) 0))
               (setf %llama-cpp:n-tokens (1- count))
               (%llama-cpp:decode context batch)
               ;; one last token
               (let* ((i (1- count))
                      (last-token (mem-aref %llama-cpp:token '%llama-cpp:token i))
                      (seq-ids (mem-aref %llama-cpp:seq-id :pointer 0)))
                 (setf (mem-aref %llama-cpp:token '%llama-cpp:token 0) last-token
                       (mem-aref %llama-cpp:pos '%llama-cpp:pos 0) (+ i pos)
                       (mem-aref %llama-cpp:n-seq-id :int32 0) 1
                       (mem-aref seq-ids '%llama-cpp:seq-id 0) 0
                       (mem-aref logits :int8 0) 0)
                 (setf %llama-cpp:n-tokens 1)
                 (%llama-cpp:decode context batch))
               (setf (last-pos llama) (+ pos count)))
             (%llama-cpp:batch-free batch))))))

(defun print-tokens (tokens count context)
  (let ((model (%llama-cpp:get-model context))
        (bos (%llama-cpp:token-bos context)))
    (loop for i from 0 to (1- count)
          for token = (cffi:mem-aref tokens '%llama-cpp:token i)
          with strip-space = t
          do (if (> token 0)
                 (let ((str (detokenize model token strip-space)))
                   (format t "~a: '~a' (~{~a~^ ~}) #~a~%" i
                           str
                           (mapcar #'char-code (coerce str 'list))
                           token))
                 (format t "~a: ~a~%" i token))
          when (/= token bos)
            do (setf strip-space nil))))

(defun get-candidate (context top-k top-p temp idx)
  (let ((vocab-size (%llama-cpp:n-vocab (%llama-cpp:get-model context)))
        (logits (%llama-cpp:get-logits-ith context idx)))
    (cffi:with-foreign-objects ((data '(:struct %llama-cpp:token-data) vocab-size)
                                (candidates '(:struct %llama-cpp:token-data-array)))
      (loop for i from 0 to (1- vocab-size)
            do (cffi:with-foreign-slots ((%llama-cpp:id %llama-cpp:logit %llama-cpp:p)
                                         (cffi:mem-aref data '%llama-cpp:token-data i)
                                         %llama-cpp:token-data)
                 (setf %llama-cpp:id i
                       %llama-cpp:logit (cffi:mem-aref logits :float i)
                       %llama-cpp:p 0.0)))
      (cffi:with-foreign-slots ((%llama-cpp:data %llama-cpp:size %llama-cpp:sorted)
                                candidates %llama-cpp:token-data-array)
        (setf %llama-cpp:data data
              %llama-cpp:size vocab-size
              %llama-cpp:sorted nil))
      (%llama-cpp:sample-top-k context candidates top-k 1)
      (%llama-cpp:sample-top-p context candidates top-p 1)
      (%llama-cpp:sample-temp context candidates temp)
      (%llama-cpp:sample-token context candidates))))

(defun feed (context model text batch-size n-ctx &optional (idx 0))
  (with-foreign-object (batch '(:struct %llama-cpp:batch))
    (%llama-cpp:batch-init batch batch-size 0 1)
    (with-foreign-slots ((%llama-cpp:n-tokens
                          %llama-cpp:token
                          %llama-cpp:pos
                          %llama-cpp:n-seq-id
                          %llama-cpp:seq-id
                          (:pointer %llama-cpp:logits))
                         batch
                         %llama-cpp:batch)
      (let ((count (tokenize model text n-ctx %llama-cpp:token)))
        ;;(print-tokens %llama-cpp:token count context)
        (setf %llama-cpp:n-tokens count)
        (let ((logits (mem-ref %llama-cpp:logits :pointer)))
          (loop for i from 0 to (1- count)
                for token = (mem-aref %llama-cpp:token '%llama-cpp:token i)
                for seq-ids = (mem-aref %llama-cpp:seq-id :pointer i)
                do (setf (mem-aref %llama-cpp:pos '%llama-cpp:pos i) (+ i idx)
                         (mem-aref %llama-cpp:n-seq-id :int32 i) 1
                         (mem-aref seq-ids '%llama-cpp:seq-id 0) 0
                         (mem-aref logits :int8 i) 0))
          (setf (mem-aref logits :int8 (1- count)) 1))
        (%llama-cpp:decode context batch)
        (%llama-cpp:batch-free batch)
        count))))

(defun fill-batch (batch tokens &optional (pos 0))
  (with-foreign-slots ((%llama-cpp:n-tokens
                        %llama-cpp:token
                        %llama-cpp:pos
                        %llama-cpp:n-seq-id
                        %llama-cpp:seq-id
                        (:pointer %llama-cpp:logits))
                       batch
                       %llama-cpp:batch)
    (let ((logits (mem-ref %llama-cpp:logits :pointer))
          (count (length tokens)))
      (setf %llama-cpp:n-tokens count)
      (loop for i from 0 to (1- count)
            for token in tokens
            for seq-ids = (mem-aref %llama-cpp:seq-id :pointer i)
            do (setf (mem-aref %llama-cpp:token '%llama-cpp:token i) token
                     (mem-aref %llama-cpp:pos '%llama-cpp:pos i) (+ i pos)
                     (mem-aref %llama-cpp:n-seq-id :int32 i) 1
                     (mem-aref seq-ids '%llama-cpp:seq-id 0) 0
                     (mem-aref logits :int8 i) 0))
      (setf (mem-aref logits :int8 (1- count)) 1))))

(defvar *model*)
(defvar *context*)

(defvar *n-vocab*)

(defparameter *top-k* 40)
(defparameter *top-p* 0.9)
(defparameter *temp* 0.73)

(defun brain-init ()
  (backend-init)

  (let ((model-path (namestring (merge-pathnames *model-file* *models-path*))))
    (defvar *model* (cl-llama:load-model model-path *model-params*)))

  (defparameter *n-vocab* (%llama-cpp:n-vocab *model*))
  (format t "vocab size: ~a~%" *n-vocab*)

  (defvar *context* (create-context *model* *context-params*)))

(defun brain-deinit ()
  (free-context *context*)
  (free-model *model*)
  (backend-free))

;;; couple of utility functions
(defun warm-up (context)
  (with-foreign-object (batch '(:struct %llama-cpp:batch))
    (%llama-cpp:batch-init batch 1 0 1)
    (with-foreign-slots ((%llama-cpp:token %llama-cpp:n-tokens)
                         batch (:struct %llama-cpp:batch))
      (setf (mem-ref %llama-cpp:token '%llama-cpp:token) (%llama-cpp:token-bos context)
            %llama-cpp:n-tokens 1))
    (%llama-cpp:decode context batch)
    (%llama-cpp:batch-free batch)))

(defun batch->plist (batch)
  "Use this function instead of mem-ref since otherwise logits is considered a string"
  (with-foreign-slots ((%llama-cpp:n-tokens
                        %llama-cpp:token
                        %llama-cpp:embd
                        %llama-cpp:pos
                        %llama-cpp:seq-id
                        (:pointer %llama-cpp:logits))
                       batch %llama-cpp:batch)
    (list :n-tokens %llama-cpp:n-tokens :token %llama-cpp:token
          :embd %llama-cpp:embd :pos %llama-cpp:pos
          :seq-id %llama-cpp:seq-id :logits (mem-ref %llama-cpp:logits :pointer))))

(defun token-bytes (model token &optional strip-space)
  "Turn token into bytes. Optionally strip leading space."
  (let ((length (- (%llama-cpp:token-to-piece model token (null-pointer) 0))))
    (format t "length is ~a chars~%" length)
    (with-foreign-object (buf :char (1+ length))
      (%llama-cpp:token-to-piece model token buf length)
      (setf (mem-aref buf :char length) 0)
      (let ((initial-index (if (and strip-space (= (mem-aref buf :char) 32)) 1 0)))
        (loop for i from initial-index to length
              collect (mem-aref buf :char i))))))
